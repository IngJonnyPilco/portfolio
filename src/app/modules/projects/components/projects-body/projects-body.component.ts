import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-projects-body',
  templateUrl: './projects-body.component.html',
  styleUrls: ['./projects-body.component.scss']
})
export class ProjectsBodyComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  public proyectWebs  = [
    {
      web:'../../../../../assets/fullwebs/carmap.webp',
      type: 'Página Web ',
    },
    {
      web:'../../../../../assets/fullwebs/magiaverdeshop.webp',
      type: 'Comercio electrónico',
    },
    {
      web:'../../../../../assets/fullwebs/riollantaFullPage (1).webp',
      type: 'Comercio electrónico',
    },
    {
      web:'../../../../../assets/fullwebs/tendenciasesteticaintegral (1).webp',
      type: 'Pagina web ',
    },
    {
      web:'../../../../../assets/fullwebs/tendenciasesteticaintegral Landing.webp',
      type: 'Página de aterrizaje',
    },
    {
      web:'../../../../../assets/fullwebs/odisearace.webp',
      type: 'Página de aterrizaje',
    },

  ]

}
