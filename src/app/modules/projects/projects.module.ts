import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectsRoutingModule } from './projects-routing.module';
import { ProjectsLayoutComponent } from './projects-layout/projects-layout.component';
import { PageComponent } from './page/page.component';
import { SharedModule } from '@shared/shared.module';
import { ProjectsBodyComponent } from './components/projects-body/projects-body.component';


@NgModule({
  declarations: [
    PageComponent,
    ProjectsLayoutComponent,
    ProjectsBodyComponent
  ],
  imports: [
    CommonModule,
    ProjectsRoutingModule,
    SharedModule
  ]
})
export class ProjectsModule { }
