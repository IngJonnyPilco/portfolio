import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BannerComponent } from './components/banner/banner.component';
import { CotizationLayoutComponent } from './cotization-layout/cotization-layout.component';
import { HomeLayoutComponent } from './home-layout/home-layout.component';
import { PageComponent } from './page/page.component';

const routes: Routes = [
  {
    path: '',
    component: PageComponent,
    children: [
      {
        path: '',
        component:HomeLayoutComponent
      },
      {
        path: 'mona',
        component: BannerComponent
      },
      {
        path: 'cotization',
        component: CotizationLayoutComponent
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
