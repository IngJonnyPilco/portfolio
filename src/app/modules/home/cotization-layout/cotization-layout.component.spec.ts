import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CotizationLayoutComponent } from './cotization-layout.component';

describe('CotizationLayoutComponent', () => {
  let component: CotizationLayoutComponent;
  let fixture: ComponentFixture<CotizationLayoutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CotizationLayoutComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CotizationLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
