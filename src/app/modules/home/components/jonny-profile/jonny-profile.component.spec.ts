import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JonnyProfileComponent } from './jonny-profile.component';

describe('JonnyProfileComponent', () => {
  let component: JonnyProfileComponent;
  let fixture: ComponentFixture<JonnyProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JonnyProfileComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(JonnyProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
