import { AfterViewInit, Component, ElementRef, HostListener, Input, OnInit, Renderer2, ViewChild } from '@angular/core';
import * as THREE from "three";
import * as dat from "dat.gui";
import * as CANNON from "cannon-es"

import gsap from 'gsap';

import { GLTFLoader, GLTF } from 'three/examples/jsm/loaders/GLTFLoader.js';
import { RGBELoader } from 'three/examples/jsm/loaders/RGBELoader';
import { FontLoader } from 'three/examples/jsm/loaders/FontLoader';
import { TextGeometry } from 'three/examples/jsm/geometries/TextGeometry';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import { PointerLockControls } from 'three/examples/jsm/controls/PointerLockControls.js';
import { FirstPersonControls } from 'three/examples/jsm/controls/FirstPersonControls';
import { CSS2DRenderer, CSS2DObject } from 'three/examples/jsm/renderers/CSS2DRenderer.js';
import { NgxSpinnerService } from 'ngx-spinner';




@Component({
  selector: 'app-jonny-profile',
  templateUrl: './jonny-profile.component.html',
  styleUrls: ['./jonny-profile.component.scss']
})
export class JonnyProfileComponent implements OnInit, AfterViewInit  {


  @ViewChild('portfolioProfile') private canvasRef!: ElementRef<HTMLCanvasElement>; //#canvas
  // @ViewChild('contentAbout') private About!: ElementRef<HTMLElement>;
  // @ViewChild('contentHabilities') private Habilities!: ElementRef<HTMLElement>;

  public photoPerfil : string = "../../../../../assets/fotoIngJonny.webp"
  public photoPerfilFrontal : string = "../../../../../assets/jonnyDeFrente.webp"
  public fondo : string = "../../../../../assets/fondo.webp"
  constructor(private rendererInDOM: Renderer2, private elemet: ElementRef, private spinner: NgxSpinnerService){
  }
  
  //* elements 
  @Input() public estrellas: string = "/assets/img-solar-sistem/stars.jpg";
  @Input() public backgroundSimple: string = "/assets/bakcgroundBlack.webp";
  @Input() public fontMontserrat: string = "/assets/fonts/Montserrat_Thin_Regular.json";
  //* Figures3D
  @Input() public humanFigure: string = "/assets/glTF/humanJonny/scene.gltf";
  @Input() public figureWithEntrada: string = "/assets/glTF/person1.gltf";

  //* Camera Properties 
  @Input() public fieldOfView: number = 80; // FOV is the extent of the scene that is seen on the display at any given moment. The value is in degrees.
  @Input('nearClipping') public nearClippingPlane: number = 1;
  @Input('farClipping') public farClippingPlane: number = 1000;

  //* Lights 
  private ambientLight!: THREE.AmbientLight;
  private lightSun!: THREE.PointLight;
  private directionalLight!: THREE.DirectionalLight;
  private hemiLight!: THREE.HemisphereLight
  private spotLight!: THREE.SpotLight

  //*LightsHelpers
  private dirLightHelper!: THREE.DirectionalLightHelper
  private dirLightShadowHelper!: THREE.CameraHelper
  private spotLightHelper!: THREE.SpotLightHelper

  //*helpers
  private axesHelpers  =  new THREE.AxesHelper(10)
  private gridHelper = new THREE.GridHelper(20,20)

  //* controls
  private controls!: OrbitControls;
  private pLControls! : PointerLockControls;

  //* animations
  private mixer!: THREE.AnimationMixer  
  //*------------------------------------*variables para asignar--------------------------------------------------
  private camera!: THREE.PerspectiveCamera;
  private orthographicCamera!: THREE.OrthographicCamera;
  private loadingManager: THREE.LoadingManager = new THREE.LoadingManager()
  
  private textureLoader : THREE.TextureLoader = new THREE.TextureLoader()
  private cubeTextureLoader : THREE.CubeTextureLoader = new THREE.CubeTextureLoader() 
  private fontLoader = new FontLoader();

  
  private gltfloader: GLTFLoader = new GLTFLoader(this.loadingManager);
  private renderer!: THREE.WebGLRenderer;
  private scene: THREE.Scene = new THREE.Scene();
  
  private model! : THREE.Group
  //*------------------------------------*Objetos----------------------------------------------------------------

  private planeMesh = new THREE.Mesh(
    new THREE.PlaneGeometry(20,20),
    new THREE.MeshStandardMaterial({ 
      color: 0x333333, side: THREE.DoubleSide 
    })
  );

  // private cubeMesh = new THREE.Mesh(
  //   new THREE.BoxGeometry(2, 2, 2), 
  //   new THREE.MeshBasicMaterial({
  //     color: 0x00ff
  //   })
  //   );
  //!_______________________FOR BACKGROUND_______________________ 

  // private BigSphereMesh = new THREE.Mesh(
  //   new THREE.SphereGeometry( 500, 20, 20 ),
  //   new THREE.MeshBasicMaterial( {
  //     map: this.textureLoader.load(this.backgroundSimple),
  //     side: THREE.BackSide
  //   })
  // )

  // private geometry = new THREE.SphereGeometry( 1, 20, 20 );
  //!_____________________________________________________________ 

  //?------------------------------------------------- --------------------------------------

  //*______________________________________________________________________________________________
  private contentHTMLRender = new CSS2DRenderer()
  
  //*------------------------------------*Métodos----------------------------------------------------------------
  private get canvas(): HTMLCanvasElement {
    return this.canvasRef.nativeElement;
  }
  
  private getAspectRatio() {
    return this.canvas.clientWidth / this.canvas.clientHeight;
  }

  private elementToScreen(element: HTMLElement){
    element.style.width = '100%'
    element.style.height = '100%'
  }

  private get objRender(): CSS2DRenderer{
    const rendererObj = new CSS2DRenderer();
    rendererObj.setSize(this.canvas.clientWidth, this.canvas.clientHeight);
    rendererObj.domElement.style.position = 'absolute';
    rendererObj.domElement.style.top = '0px';
    this.elementToScreen(rendererObj.domElement)
    this.rendererInDOM.appendChild(document.body, rendererObj.domElement);
    return rendererObj
  }

  private createHTMLwithPosition(...elements:ElementsToRenderHTML[]){

    for(const {elementHTML, pos} of elements) {
      const render2D = new CSS2DObject(elementHTML)
      this.scene.add(render2D)
      render2D.position.set(pos.x, pos.y, pos.z) 
    }
  }

  private moveCamera(x:number,y:number,z:number){
    // this.camera.position.x= 0 // +izquierda - derecha  //! rojo
    // this.camera.position.y= 1 // +arriba - abajo  //* verde
    // this.camera.position.z= 7 // +atras - adelante  //? azul
    gsap.to(this.camera.position,{x,y,z,duration:1.5})
  }
  private rotateCamera(x:number,y:number,z:number){
    // this.camera.rotation.x= 0.5 * Math.PI // +90 mira de  lado a la izquierda -90 mira de lado a la derecha //! rojo mirada de lado
    // this.camera.rotation.y= 0.5 * Math.PI // +90 a la izquierda -90 a la derecha  //* verde mirada al frente
    // this.camera.rotation.z= 0.5 * Math.PI  // +vuelta completa  //? azul cuello arriba y la sol abajo
    gsap.to(this.camera.rotation,{x,y,z,duration:3.5})
  }
  //!_______________________FOR PANTALLA_______________________ 

  public moveToSection(){
    // this.camera.position.set(10,10,0)
    // this.camera.rotation.set(0,0.5 * Math.PI,0)
    // this.camera.lookAt(0,0,0)
    this.moveCamera(20,100,0)
    this.rotateCamera(0,0.5 * Math.PI,0)
    gsap.to(this.model.rotation, {x:0,y:0.5 * Math.PI,z:0, duration:3.5})

  }
  
  private navigateToHome(){
  }
  
  private resposiveHome(){
    let windowWith = this.canvas.clientWidth

    if(windowWith <= 640){
      this.camera.position.set(0,10,10)
      return
    }
    let move = (windowWith - 640)  / (1536 - 640) * 100 // de 1 al 100%

    // this.camera.position.set(0,50,(50 - move/10 ))
    this.camera.position.set(0,10,10)
    this.camera.lookAt(new THREE.Vector3(0,0,0)); //hacia que punto en especifico debe ver la cámara

  }
  //!_______________________FOR PERSONAJES_______________________ 

  private insertPersonaje(){
    this.gltfloader.load(this.figureWithEntrada, (gltf: GLTF) => {
      console.log(gltf);
      this.model = gltf.scene
      this.scene.add(this.model)   
      // this.model.scale.set(.01,.01,.01)
      this.model.position.set(0,0,0)
      this.model.rotateX(-0.5 * Math.PI)
      
      // const gltfAnimations: THREE.AnimationClip[] = gltf.animations;
      // const mixer = new THREE.AnimationMixer(this.model);
      // const animationsMap: Map<string, THREE.AnimationAction> = new Map()
      // gltfAnimations.filter(a => a.name != 'TPose').forEach((a: THREE.AnimationClip) => {
      //     animationsMap.set(a.name, mixer.clipAction(a))
      // })

      this.mixer = new THREE.AnimationMixer(this.model)
      const clips = gltf.animations
      const clip = THREE.AnimationClip.findByName(clips,"hip hop")
      const action  = this.mixer.clipAction(clip)
      action.play()
      
    });

  }
  //!_______________________FOR BACKGROUND_______________________ 
  private resizeBackground(bgTexture:any){
    const canvasAspect = this.canvas.clientWidth / this.canvas.clientHeight;
    const imageAspect = bgTexture.image ? bgTexture.image.width / bgTexture.image.height : 1;
    const aspect = imageAspect / canvasAspect;
    bgTexture.offset.x = aspect > 1 ? (1 - 1 / aspect) / 2 : 0;
    bgTexture.repeat.x = aspect > 1 ? 1 / aspect : 1;
    bgTexture.offset.y = aspect > 1 ? 0 : (1 - aspect) / 2;
    bgTexture.repeat.y = aspect > 1 ? 1 : aspect;
  }

  private getRandom(): number{
    let num = Math.floor(Math.random()*500) + 1; // this will get a number between 1 and x;
    num *= Math.floor(Math.random()*2) === 1 ? 1 : -1; // this will add minus sign in 50% of cases
    return num;
  }

  private animateStarts(){
    for (let k = 0; k < this.stars.length; k++) {
      let star = this.stars[k];
      star.rotation.x += 0.01;
      star.rotation.y += 0.01;
    }
  }

  private stars:any[] = [];

  private createStarts(){
    for (let i = 0; i < 500; i++) {
      let geometry = new THREE.CircleGeometry( 1, 20 );
      let material = new THREE.MeshBasicMaterial( { color: "#fff", opacity:.6, transparent:true } );
      let star = new THREE.Mesh( geometry, material );
      star.position.set( this.getRandom(), this.getRandom(), this.getRandom() );
      star.material.side = THREE.DoubleSide;
      this.stars.push( star );
      this.scene.add( star );
   }
  }


  //!_____________________________________________________________ 
  
  //*------------------------------------*Host Listener-----------------------------------------------------------
  @HostListener('window:resize') onResizeEvent(){
    this.camera.aspect = this.getAspectRatio()
    this.camera.updateProjectionMatrix();
    // 0-768px 
    (window.innerWidth <= 640 )? 
    this.renderer.setSize(window.innerWidth - 18, window.innerHeight/2 - 100): 
    this.renderer.setSize(window.innerWidth *2/5, window.innerHeight*2/5) 

    // this.contentHTMLRender.setSize(window.innerWidth , window.innerHeight );
    // this.elementToScreen(this.contentHTMLRender.domElement)
    // this.controls.update()

    this.navigateToHome()
    this.resposiveHome()
  }

  @HostListener('mouseup',['$event']) onclick (event:Event) {
    console.log(event);
  }

  //*------------------------------------*Scene-------------------------------------------------------------------
  private createScene(){
    // this.scene.background = new THREE.Color(0x000000)

    this.createStarts()

    //*____________________________________  
    //*fonts
    // this.fontLoader.load( this.fontMontserrat,(font)=>{ 
    //   let textGeometry = new TextGeometry('Portafolio', {
    //     font: font,
    //     size: 1,
    //     height: 0.1
    //   });
    //   let fontMaterial = new THREE.MeshBasicMaterial({color: 0x00ff00});
    //   let text = new THREE.Mesh(textGeometry, fontMaterial);
    //   this.scene.add(text)
    // }) 

    //*____________________________________ 
    let aspectRatio = this.getAspectRatio();

    this.camera = new THREE.PerspectiveCamera(this.fieldOfView, aspectRatio, this.nearClippingPlane, this.farClippingPlane)

    this.resposiveHome()
    // this.camera.clearViewOffset()
    // this.camera.position.x= 0 // +izquierda - derecha  //! rojo
    // this.camera.position.y= 100// +arriba - abajo  //* verde
    // this.camera.position.z= 20 // +atras - adelante  //? azul
    // this.camera.lookAt(new THREE.Vector3(0,100, 0)); //hacia que punto en especifico debe ver la cámara
    //*____________________________________ 
    //* add objects
    
    //* figures3D 

    this.insertPersonaje()

    //* figures 
    // this.cubeMesh.position.y = 1
    //* Plane
    this.scene.add(this.planeMesh)
    this.planeMesh.rotation.set(-0.5*Math.PI,0,0)

    //*Lights
    this.ambientLight = new THREE.AmbientLight(0x333333);
    this.scene.add(this.ambientLight)

    // this.lightSun = new THREE.PointLight(0xFFFFFF, 2.5);
    // this.lightSun.position.set(0,10,10)
    // this.scene.add(this.lightSun)

    // this.directionalLight = new THREE.DirectionalLight(0XFFFFFF, 1)
    // this.directionalLight.position.set(0,25,25)
    // this.directionalLight.lookAt(0,0,0)
    // this.scene.add(this.directionalLight)
    
    this.spotLight = new THREE.SpotLight(0xffa95c,4)
    this.spotLight.castShadow = true

    this.hemiLight = new THREE.HemisphereLight(0xffeeb1,0x080820, 4)
    this.scene.add(this.hemiLight)
    
    //* add helpers
    this.scene.add(this.axesHelpers, this.gridHelper)
    
    //* light helpers
    // this.dirLightHelper = new THREE.DirectionalLightHelper(this.directionalLight,1) 
    // this.scene.add(this.dirLightHelper)

    // this.spotLightHelper = new THREE.SpotLightHelper(this.spotLight)
    // this.scene.add(this.spotLight,this.spotLightHelper)

    //*Renders
    // this.contentHTMLRender = this.objRender
    // this.createHTMLwithPosition( 
    //   { elementHTML : this.About.nativeElement, pos:{ x:17, y:100, z:0} }, 
    //   { elementHTML : this.Habilities.nativeElement, pos:{ x:0, y:0, z:5} }, 
    // )
  }
  
  //*------------------------------------*Render-------------------------------------------------------------------
  private clock = new THREE.Clock()
  //* 
  private starRender(){
    this.renderer = new THREE.WebGLRenderer({ canvas: this.canvas, antialias:true });
    // this.renderer.autoClearColor = false
    this.renderer.toneMapping = THREE.ReinhardToneMapping
    this.renderer.toneMappingExposure = 2.3
    this.renderer.setPixelRatio(devicePixelRatio);
    this.renderer.setSize(this.canvas.clientWidth -20, this.canvas.clientHeight);
    // const bgTexture = this.textureLoader.load(this.backgroundSimple);
    // this.scene.background = bgTexture;

    this.renderer.setAnimationLoop(()=>{
      this.renderer.render(this.scene, this.camera);
      // this.contentHTMLRender.render(this.scene, this.camera);
      this.animateStarts()
      if(this.mixer){
        this.mixer.update(this.clock.getDelta())
      }
      // this.resizeBackground(bgTexture)
    });
  }


  //*------------------------------------*PointerLockControls--//uso del raton para mover la camara------------------
  private createPointerControls(){
    const controlsPointerHTML = this.objRender.domElement
    controlsPointerHTML.style.zIndex = '-1'
    this.pLControls = new PointerLockControls(this.camera, controlsPointerHTML)
  }

  //*------------------------------------*controls-------------------------------------------------------------------
  private createControls () {
    const controlsHTML = this.objRender.domElement
    controlsHTML.style.zIndex = '100'
    this.controls = new OrbitControls(this.camera, controlsHTML);
    // this.controls.panSpeed = 2
    // this.controls.rotateSpeed = 2
    // this.controls.maxDistance = 50
    // this.controls.autoRotate = true;
    // this.controls.autoRotateSpeed= 5
    // this.controls.target = new THREE.Vector3(2,2,2)
    // this.controls.enableZoom = true;
    // this.controls.enablePan = false;
    // this.controls.enableDamping= true
    // this.controls.dampingFactor = 0.02
    // this.controls.mouseButtons.LEFT =  THREE.MOUSE.ROTATE
    // this.controls.mouseButtons.RIGHT = THREE.MOUSE.PAN
    // this.controls.keys = {
    //   LEFT: 'ArrowLeft',
    //   UP: 'ArrowUp',
    //   RIGHT: 'ArrowRight',
    //   BOTTOM: 'ArrowBottom'
    // }
    // this.controls.listenToKeyEvents(window)
    this.controls.update();
  };
  

  ngAfterViewInit() {
    this.createScene();
    this.starRender();
    // this.createControls();
  }
  
  ngOnInit(): void {
    this.loadingManager.onProgress = (url,loaded,total) =>{
      let loadProgres = (loaded / total)*100
      this.spinner.show();
    }
    this.loadingManager.onLoad = () => {
      console.log('pagina cargada');
      this.spinner.hide();
    } 
  }
  

  public tecnologiOptions: NavigationMenu[] = [
    { 
      navigation: 'Angular',
      icon: '/assets/icons/angular-svgrepo-com.svg',
    },
    { 
      navigation: 'Django Rest Framework',
      icon: '/assets/icons/django-svgrepo-com.svg',
    },
    { 
      navigation: 'NestJs',
      icon: '/assets/icons/nest-middleware-ts-svgrepo-com.svg',
    },
    { 
      navigation: 'SQL',
      icon: '/assets/icons/sql-database-sql-azure-svgrepo-com.svg',
    }
  ]

  navigation(){
    console.log('click');
  }

}

interface NavigationMenu { 
    navigation: string,
    icon: string,
}

interface ElementsToRenderHTML {
  elementHTML: HTMLElement,
  pos : {
    x:number,
    y:number,
    z:number
  }
}