import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-work',
  templateUrl: './my-work.component.html',
  styleUrls: ['./my-work.component.scss']
})
export class MyWorkComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  public proyects  = [
    '../../../../../assets/Websmini/carmap.webp',
    '../../../../../assets/Websmini/landing.webp',
    '../../../../../assets/Websmini/magiaverdeshop.webp',
    '../../../../../assets/Websmini/riollanta.webp',
    '../../../../../assets/Websmini/WebTendenciaSteticaIntegral.webp',

  ]
}
