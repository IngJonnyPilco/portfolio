import { AfterViewInit, Component, ElementRef, HostListener, Input, OnInit, QueryList, Renderer2, ViewChild, ViewChildren } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import * as THREE from "three";
import * as dat from "dat.gui";
import * as CANNON from "cannon-es"

import gsap from 'gsap';

import { GLTFLoader, GLTF } from 'three/examples/jsm/loaders/GLTFLoader.js';
import { RGBELoader } from 'three/examples/jsm/loaders/RGBELoader';
import { FontLoader } from 'three/examples/jsm/loaders/FontLoader';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import { FirstPersonControls } from 'three/examples/jsm/controls/FirstPersonControls';
import { CSS2DRenderer } from 'three/examples/jsm/renderers/CSS2DRenderer.js';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit, AfterViewInit {

  @ViewChild('canvasElement') private canvasRef!: ElementRef<HTMLCanvasElement>; //#canvas

  //*background 
  @Input() public backgroundTexture: string = "/assets/GALAXY.jpg";
  @Input() public stars: string = "/assets/background.jpg";

  //* elements 
  @Input() public sun: string = "/assets/img-solar-sistem/sun.jpg";
  @Input() public mercuryTexture: string = "/assets/img-solar-sistem/mercury.jpg";
  @Input() public saturnoTexture: string = "/assets/img-solar-sistem/saturn.jpg";
  @Input() public saturnoRingTexture: string = "/assets/img-solar-sistem/saturn_ring.png";
  @Input() public venusTexture: string = "/assets/img-solar-sistem/venus.jpg";
  @Input() public earthTexture: string = "/assets/img-solar-sistem/earth.jpg";
  @Input() public estrellas: string = "/assets/img-solar-sistem/stars.jpg";
  @Input() public uranusTexture: string = "/assets/img-solar-sistem/uranus.jpg";
  @Input() public uranus_ring: string = "/assets/img-solar-sistem/uranus_ring.jpg";

  //* Cube Properties
  @Input() public rotationSpeedX: number = 0.05;
  @Input() public rotationSpeedY: number = 0.01;
  @Input() public size: number = 200;
  @Input() public texture: string = "/assets/texture.jpg";

  //* Sphere Properties 
  @Input() public step: number = 0;
  @Input() public speed: number = 0.01;

  //* Stage Properties
  // @Input() public cameraX: number = 40;  //!rojo
  // @Input() public cameraY: number = 40;   //*verde
  // @Input() public cameraZ: number = 0;   //?azul

  //* Camera Properties 
  @Input() public fieldOfView: number = 45; // FOV is the extent of the scene that is seen on the display at any given moment. The value is in degrees.
  @Input('nearClipping') public nearClippingPlane: number = 0.1;
  @Input('farClipping') public farClippingPlane: number = 1000;

  //* Lights 
  private ambientLight!: THREE.AmbientLight;
  private lightSun!: THREE.PointLight;
  private directionalLight!: THREE.DirectionalLight;

  private spotLight!: THREE.SpotLight
  
  //*LightsHelpers
  private dirLightHelper!: THREE.DirectionalLightHelper
  private dirLightShadowHelper!: THREE.CameraHelper
  private spotLightHelper!: THREE.SpotLightHelper

  //* FOG
  private fog!: THREE.Fog 
  //* options for gui
  private options = {
    sphereColor : '#ffea00',
    wireframe: false,
    speed: 0.01,
    angle: 0.2,
    penumbra: 0,
    intensity: 1,
  }

  //* CANNONJS para aplicar leyes de la física 
  private groundPhysMat = new CANNON.Material()

  private world : CANNON.World = new CANNON.World({
    gravity: new CANNON.Vec3(0, -9.8, 0)
  })

  private groundBody = new CANNON.Body({
    // shape: new CANNON.Plane(),
    shape: new CANNON.Box(new CANNON.Vec3(15,15,0.1)),
    // mass: 10
    type: CANNON.Body.STATIC,
    material: this.groundPhysMat
  })

  private timeStep:number = 1/60

  //*------------------------------------*variables para asignar--------------------------------------------------
  private camera!: THREE.PerspectiveCamera;
  // private backgroundLoader : THREE.TextureLoader = new THREE.TextureLoader()
  private loadingManager: THREE.LoadingManager = new THREE.LoadingManager()
  private loader : THREE.TextureLoader = new THREE.TextureLoader()
  private cubeTextureLoader : THREE.CubeTextureLoader = new THREE.CubeTextureLoader() 
  private gltfloader: GLTFLoader = new GLTFLoader(this.loadingManager);

  private Fontsloader : FontLoader = new FontLoader();
 
  private mousePosition : THREE.Vector2 = new THREE.Vector2()
  private rayCaster : THREE.Raycaster = new THREE.Raycaster()
  private renderer!: THREE.WebGLRenderer;
  private scene: THREE.Scene = new THREE.Scene();
  
  //*helpers
  private axesHelpers  =  new THREE.AxesHelper(10)
  private gridHelper = new THREE.GridHelper(30,30)
  private grid2Helper = new THREE.GridHelper(20,20)
  //* controls
  private controls!: OrbitControls;
  private controlsFPC! : FirstPersonControls

  //*cargar objeto gltf 
  private model!: THREE.Group;

  
  //?---------------------------------------OBJETOS----------------------------------- 
  private cubePhysMat = new CANNON.Material()
  //?----------------------------------------------------------------------------------
  private cubeGeo = new THREE.BoxGeometry(2, 2, 2);
  // private material = new THREE.MeshBasicMaterial({ map: this.loader.load(this.texture) });
  private cubeMat = new THREE.MeshBasicMaterial({ color: 0xff002d });
  private cubeMesh: THREE.Mesh = new THREE.Mesh(this.cubeGeo, this.cubeMat);
  private cubeBody = new CANNON.Body({
    mass: 1,
    shape: new CANNON.Box(new CANNON.Vec3(1,1,1)),
    position: new CANNON.Vec3(2,20,0),
    material: this.cubePhysMat
  })
  private groundCubeContactMat = new CANNON.ContactMaterial(
    this.groundPhysMat,
    this.cubePhysMat,
    {
      friction:0.04
    }
  )
  //?----------------------------------------------------------------------------------
  private spherePhysMath = new CANNON.Material()
  //?----------------------------------------------------------------------------------
  private spheOverGeo = new THREE.SphereGeometry(1.75, 50,50);
  private spheOverMAT = new THREE.MeshStandardMaterial({color: 0X0000FF, wireframe:false})
  private sphereMesh = new THREE.Mesh(this.spheOverGeo, this. spheOverMAT)
  private sphereBody = new CANNON.Body({
    mass: 1,
    shape: new CANNON.Sphere(1.75),
    position: new CANNON.Vec3(-2,10,0),
    material: this.spherePhysMath
  })

  private groundSphereContactMat = new CANNON.ContactMaterial(
    this.groundPhysMat,
    this.spherePhysMath,
    {
      restitution:0.9
    }
  )
  

  //?---------------------------------------------------------------------------------- 
  private geometry2 = new THREE.BoxGeometry(2.5, 2.5, 2.5);
  private geometry2Multimaterial =  [
    new THREE.MeshBasicMaterial({ map: this.loader.load(this.stars) }),
    new THREE.MeshBasicMaterial({ map: this.loader.load(this.backgroundTexture) }),
    new THREE.MeshBasicMaterial({ map: this.loader.load(this.stars) }),
    new THREE.MeshBasicMaterial({ map: this.loader.load(this.stars) }),
    new THREE.MeshBasicMaterial({ map: this.loader.load(this.backgroundTexture) }),
    new THREE.MeshBasicMaterial({ map: this.loader.load(this.backgroundTexture) }),
  ]
  private cubeWithTexture: THREE.Mesh = new THREE.Mesh(this.geometry2, this.geometry2Multimaterial);
  
  //?------------------------------------------------- --------------------------------------
  private planeGeometry = new THREE.PlaneGeometry(30,30)
  private planeMaterial = new THREE.MeshStandardMaterial({ color: 0xffffff, side: THREE.DoubleSide, visible: false });
  private plane = new THREE.Mesh(this.planeGeometry, this.planeMaterial)
  
  //?--------------------------------------------------------------------------------------- 
  private plane2Geometry = new THREE.PlaneGeometry(10,10,10,10)
  private plane2Material = new THREE.MeshStandardMaterial({ color: 0xffffff, wireframe:true });
  private plane2 = new THREE.Mesh(this.plane2Geometry, this.plane2Material)
  
  //?--------------------------------------------------------------------------------------- 
  // private plane3Ge = new THREE.PlaneGeometry(20,20)
  // private plane3Ma = new THREE.MeshStandardMaterial({ color: 0xffffff, side:DoubleSide});
  // private plane3Mesh = new THREE.Mesh(this.plane3Ge, this.plane3Ma)
  
  //?--------------------------------------------------------------------------------------- 
  private sphereGeometry = new THREE.SphereGeometry(1, 50,50);
  private sphereMaterial = new THREE.MeshStandardMaterial({color: 0X0000FF})
  private sphere = new THREE.Mesh(this.sphereGeometry, this.sphereMaterial)
  private sphereID = this.sphere.id

  //?--------------------------------------------------------------------------------------- 

  private sphereClickMesh = new THREE.Mesh(
    new THREE.SphereGeometry(0.4,4,2),
    new THREE.MeshBasicMaterial({wireframe:true, color:0Xffffff})
  )

  //?--------------------------------------------------------------------------------------- 

  private sunGeo = new THREE.SphereGeometry(2.5,50,50)
  private sunGeoMaterial = new THREE.MeshBasicMaterial({
    map: this.loader.load(this.sun)
  })
  private sunObject = new THREE.Mesh(this.sunGeo, this.sunGeoMaterial)
  //?--------------------------------------------------------------------------------------- 

  //! tareas reemplazadas por la  funcion createPlanet 
  // private mercuryGeo = new THREE.SphereGeometry(0.3,50,50)
  // private mercuryGeoMaterial = new THREE.MeshStandardMaterial({
  //   map: this.loader.load(this.mercuryTexture)
  // })
  // private mercury = new THREE.Mesh(this.mercuryGeo, this.mercuryGeoMaterial)
  // private mercuryObj = new THREE.Object3D()
  private mercury = this.createPlanet(0.3,this.mercuryTexture,5)
  //?--------------------------------------------------------------------------------------- 

  //! tareas reemplazadas por la  funcion createPlanet 
  // private saturnoGeo = new THREE.SphereGeometry(1,50,50)
  // private saturnoGeoMaterial = new THREE.MeshStandardMaterial({
  //   map: this.loader.load(this.saturnoTexture)
  // })
  // private saturno = new THREE.Mesh(this.saturnoGeo, this.saturnoGeoMaterial)
  // private saturnoObj = new THREE.Object3D()
  private saturno = this.createPlanet(1,this.saturnoTexture,10,{innerRadius:1, outerRadius:2,texture:this.saturnoRingTexture})
  //?--------------------------------------------------------------------------------------- 

  //! tareas reemplazadas por la  funcion createPlanet 
  // private saturnoRingGeo = new THREE.RingGeometry(1,2,50)
  // private saturnoRingGeoMaterial = new THREE.MeshBasicMaterial({
  //   map: this.loader.load(this.saturnoRingTexture),
  //   side: THREE.DoubleSide
  // })
  // private saturnoRing = new THREE.Mesh(this.saturnoRingGeo, this.saturnoRingGeoMaterial)

  //?--------------------------------------------------------------------------------------- 

  private groundGeo = new THREE.PlaneGeometry(30,30)
  private groundMath = new THREE.MeshBasicMaterial({
    color: 0xffffff,
    side: THREE.DoubleSide,
    wireframe: true
  })
  private groundMesh = new THREE.Mesh(this.groundGeo, this.groundMath)
  //?--------------------------------------------------------------------------------------- 

  private highlightMesh = new THREE.Mesh(
    new THREE.PlaneGeometry(1,1),
    new THREE.MeshBasicMaterial({
      side:THREE.DoubleSide,
      transparent:true
    })
  )

  //?--------------------------------------------------------------------------------------- 
    // how to insert text ?
    

  //*--------------------------------------------------------------------------------------
  
  //* Methods Used

  private get canvas(): HTMLCanvasElement {
    return this.canvasRef.nativeElement;
  }

  private get objRender(): CSS2DRenderer{
    const renderer = new CSS2DRenderer();
    renderer.setSize(this.canvas.clientWidth, this.canvas.clientHeight);
    renderer.domElement.style.position = 'absolute';
    renderer.domElement.style.top = '0px';
    this.rendererInDOM.appendChild(document.body, renderer.domElement);
    return renderer
  }

  private getAspectRatio() {
    return this.canvas.clientWidth / this.canvas.clientHeight;
  }
  
  private animateCube() {
    this.cubeMesh.rotation.x += this.rotationSpeedX;
    this.cubeMesh.rotation.y += this.rotationSpeedY;
  }

  private animateSphere(){
    this.step += this.options.speed
    this.sphere.position.y = 10 * Math.abs(Math.sin(this.step))
  }
  
  private spotLightOptions(){
    this.spotLight.angle = this.options.angle
    this.spotLight.penumbra = this.options.penumbra
    this.spotLight.intensity = this.options.intensity
    this.spotLightHelper.update()
  }

  @HostListener('window:mousemove', ['$event']) onMouseMove(event:MouseEvent) {
    this.mousePosition.x = (event.clientX / this.canvas.clientWidth) * 2 - 1
    this.mousePosition.y = - (event.clientY / this.canvas.clientHeight) * 2 + 1
  }

  @HostListener('window:resize') onResizeEvent(){
    const aspectRatio = window.innerWidth / window.innerHeight
    this.camera.aspect = aspectRatio
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(window.innerWidth , window.innerHeight); //actualizamos  las medidas
    // this.controls.update()
  }


  private timeline = gsap.timeline()
  private objects : any[] = []
  @HostListener('window:mousedown') clickEvent(){
    this.rayCaster.setFromCamera(this.mousePosition,this.camera)
    const intersects: any = this.rayCaster.intersectObjects(this.scene.children)
    const objectExist = this.objects.find( (object:any)=> {
     return (object.position.x === this.highlightMesh.position.x) && (object.position.z === this.highlightMesh.position.z)
    })

    if(!objectExist) {
      intersects.forEach((intersect:any) => {
        if(intersect.object.name === "groundPlane"){
          const sphereClone = this.sphereClickMesh.clone();
          sphereClone.position.copy(this.highlightMesh.position);
          this.scene.add(sphereClone)
          this.objects.push(sphereClone)
          this.highlightMesh.material.color.setHex(0xFF0000)
        }
      });
    }
  }

  private position :number = 0
  @HostListener('mouseup', ['$event']) onMouseUp(event: MouseEvent){

    console.log(this.canvas.id)
    switch(this.position){
      case 0:
        this.moveCamera(0,25,20)
        this.rotateCamera(-0.3,0,0)
        this.position = 1
        break;
      case 1:
        this.moveCamera(0,20,10)
        this.rotateCamera(-0.2,0,0)
        this.position = 2
        break;
      case 2:
        this.moveCamera(0,10,10)
        this.rotateCamera(-0.5,0,0)
        this.position = 0
        break;
    }
  }

  private count = 0
  // @HostListener('window:scroll', ['$event'])   onScroll(event: Event) {
  //   this.count++
  // }

  private moveCamera(x:number,y:number,z:number){
    gsap.to(this.camera.position,{x,y,z,duration:1.5})
  }
  private rotateCamera(x:number,y:number,z:number){
    gsap.to(this.camera.rotation,{x,y,z,duration:3.5})
  }

  private rayCasterFunc(){
    this.cubeWithTexture.name = "theBox"
    this.plane.name = "groundPlane"
    this.rayCaster.setFromCamera(this.mousePosition,this.camera)
    var intersects: any = this.rayCaster.intersectObjects(this.scene.children)
    
    for(let i =0; i<intersects.length;i++) {

      if(intersects[i].object.id === this.sphereID){
        intersects[i].object.material.color.set(0xffffff)
      }
      if(intersects[i].object.name === "theBox"){
          intersects[i].object.rotation.x += this.rotationSpeedX;
          intersects[i].object.rotation.y += this.rotationSpeedY;
      }
    }
    
    const objectExist = this.objects.find( (object:any)=> {
      return (object.position.x === this.highlightMesh.position.x) && (object.position.z === this.highlightMesh.position.z)
     })

    intersects.forEach((intersect:any) => {
      if(intersect.object.name === "groundPlane"){
        const highLightPos = new THREE.Vector3().copy(intersect.point).floor().addScalar(0.5)
        this.highlightMesh.position.set(highLightPos.x, 0, highLightPos.z) 
        if(!objectExist){
          this.highlightMesh.material.color.setHex(0xFFFFFF)
        }else {
          this.highlightMesh.material.color.setHex(0xFF0000)
        }
      }
    })
  }
  
  // private z: number = 0
  // private animatedCamera(){
  //   const zFinal :number =14
  //   this.z += 0.01
  //   if(this.z < zFinal) {
  //     this.camera.position.z = this.z
  //   }
  // }

  // private animateCursor(){

  // }

  private animatedVertices(){
    let x = 10 * Math.random()
    let y = 10 * Math.random()
    let z = 10 * Math.random()
    const attribute = this.plane2.geometry.attributes['position'];
    attribute.setXYZ(0,x,y,z)  
    attribute.needsUpdate = true
  }

  private rotationPlanets(){
    this.sunObject.rotateY(0.004)

    this.mercury.mesh.rotateY(0.004)
    this.mercury.planetObj.rotateY(0.004)

    this.saturno.mesh.rotateY(0.004)
    this.saturno.planetObj.rotateY(0.004)

  }

  private createPlanet(size :number, texture:string, position :number , ring?:any ){
    let planetGeo = new THREE.SphereGeometry(size,50,50)
    let planetGeoMaterial = new THREE.MeshStandardMaterial({
      map: this.loader.load(texture)
    })
    let mesh = new THREE.Mesh(planetGeo, planetGeoMaterial)
    let planetObj = new THREE.Object3D()
    mesh.position.set(position,10,0)
    planetObj.position.z = -10
    
    if (ring){
      let planetRingGeo = new THREE.RingGeometry(ring.innerRadius,ring.outerRadius,50)
      let planetRingGeoMaterial = new THREE.MeshBasicMaterial({
        map: this.loader.load(ring.texture),
        side: THREE.DoubleSide
      })
      let planetRing = new THREE.Mesh(planetRingGeo, planetRingGeoMaterial)

      planetRing.position.set(10,10,0)
      planetRing.rotation.x = -0.5 * Math.PI
      planetObj.add(planetRing)
    }

    return {mesh, planetObj}
  }

  private appyCannon(){
    this.world.step(this.timeStep)
    this.groundMesh.position.set(this.groundBody.position.x, this.groundBody.position.y, this.groundBody.position.z);
    this.groundMesh.quaternion.set(this.groundBody.quaternion.x, this.groundBody.quaternion.y, this.groundBody.quaternion.z, this.groundBody.quaternion.w);
    
    this.cubeMesh.position.set(this.cubeBody.position.x, this.cubeBody.position.y, this.cubeBody.position.z);
    this.cubeMesh.quaternion.set(this.cubeBody.quaternion.x, this.cubeBody.quaternion.y, this.cubeBody.quaternion.z, this.cubeBody.quaternion.w);
   
    this.sphereMesh.position.set(this.sphereBody.position.x, this.sphereBody.position.y, this.sphereBody.position.z);
    this.sphereMesh.quaternion.set(this.sphereBody.quaternion.x, this.sphereBody.quaternion.y, this.sphereBody.quaternion.z, this.sphereBody.quaternion.w);

  }

  private clock = new THREE.Clock();
  private handleControlFPC(){
    this.controlsFPC.update(this.clock.getDelta())
  }

  
  
  /**
   *!!!!! Aqui inicia TODO  !!!!
   *
   * @private
   * @memberof BannerComponent
   */
  
  private createScene() {

    //* Scene
    // this.scene.background = new THREE.Color(0x000000)
    // this.scene.background = this.loader.load(this.estrellas)
    this.scene.background = this.cubeTextureLoader.load([
      this.estrellas,
      this.estrellas,
      this.estrellas,
      this.estrellas,
      this.estrellas,
      this.estrellas,
    ])

    //* Texture Loader
    //? the solar sistem------
    this.scene.add(this.sunObject)
    // this.sunObject.add(this.mercury) // las siguientes 2 lineas reemplazan esto
    this.mercury.planetObj.add(this.mercury.mesh)
    this.scene.add(this.mercury.planetObj)

    this.saturno.planetObj.add(this.saturno.mesh)
    this.scene.add(this.saturno.planetObj)
    //?---------------------  

    //* Insert object through
    
    this.gltfloader.load('/assets/figureMona/scene.gltf', (gltf: GLTF) => {
      const model = gltf.scene
      this.scene.add(model)
      this.model = model
      this.model.scale.set(10,10,10)
      this.model.position.set(0,0,-16)
    });
      //* insert scena gltf
      // this.renderer.outputEncoding = THREE.sRGBEncoding;
      // this.renderer.toneMapping = THREE.ACESFilmicToneMapping;
        
      // this.gltfloader.load('/assets/the_king_s_hall/scene.gltf',  (gltf: GLTF) =>{
      //   const model = gltf.scene;
      //   this.scene.add(model);
      // })

    //*Cannon
    this.scene.add(this.groundMesh,this.sphereMesh,this.cubeMesh)
    // this.world.step(this.timeStep)
    this.world.addBody(this.groundBody)
    this.world.addBody(this.cubeBody)
    this.world.addBody(this.sphereBody)
    this.world.addContactMaterial(this.groundCubeContactMat)
    this.world.addContactMaterial(this.groundSphereContactMat)
  
    this.sphereBody.linearDamping = 0.5 // linear damping es la resistencia del aire
    this.cubeBody.angularVelocity.set(0,10,0)
    this.cubeBody.angularDamping = 0.5

    this.groundBody.quaternion.setFromEuler(-Math.PI / 2,0,0)

    //*Plane interactive 
    // this.scene.add(this.plane3Mesh) 
    // this.plane3Mesh.position.x = 30
    // this.plane3Mesh.rotateX(- Math.PI / 2)
    // this.plane3Mesh.add(this.grid2Helper.rotateX(- Math.PI / 2))
    this.highlightMesh.rotateX( - Math.PI / 2)
    this.highlightMesh.position.set(0.5,0,0.5)
    this.scene.add(this.highlightMesh)

    //*Lights
    this.ambientLight = new THREE.AmbientLight(0x333333);
    this.lightSun = new THREE.PointLight(0xFFFFFF, 5, 30);
    this.directionalLight = new THREE.DirectionalLight(0XFFFFFF,0.8)
    this.dirLightShadowHelper = new THREE.CameraHelper(this.directionalLight.shadow.camera)
    
    this.scene.add(this.ambientLight)
    // this.scene.add(this.directionalLight)
    this.scene.add(this.lightSun)
    
    this.spotLight = new THREE.SpotLight(0xffffff)
    this.spotLightHelper = new THREE.SpotLightHelper(this.spotLight)
    this.scene.add(this.spotLight,this.spotLightHelper)

    //*shadows 
    this.directionalLight.castShadow = true
    this.plane.receiveShadow = true
    this.sphere.castShadow = true
    this.spotLight.castShadow = true
    this.lightSun.castShadow = true
    this.cubeMesh.castShadow = true

    this.mercury.mesh.castShadow = true
    //* add Lights
    this.dirLightHelper = new THREE.DirectionalLightHelper(this.directionalLight,5)

    //* Add FOG
    this.fog = new THREE.Fog(0xffffff, 0, 1000)
    this.scene.fog = this.fog

    //* add Elements
    this.scene.add(this.plane, this.sphere, this.cubeWithTexture);

    //* add helpers
    this.scene.add(this.axesHelpers, this.gridHelper)
    // this.scene.add(this.dirLightHelper, this.dirLightShadowHelper)
    
    //*Camera
    let aspectRatio = this.getAspectRatio();
    this.camera = new THREE.PerspectiveCamera( this.fieldOfView, aspectRatio, this.nearClippingPlane,this.farClippingPlane)
    this.camera.position.set(30, 30, 30)
    this.camera.rotateX(-0.5)
    this.camera.rotateY(0.5)
    this.camera.rotateZ(0.3)
    //* positions 
    this.directionalLight.position.set(-20,30,0)
    this.directionalLight.shadow.camera.bottom = -10
    this.spotLight.position.set(-10,30,0)

    this.plane.rotation.x = -0.5 * Math.PI
    this.plane2.position.set(10,10,15)

    this.sphere.position.set(-10,10,0)
    this.cubeWithTexture.position.set(0,10,10)

    //? position solarsistem
    this.sunObject.position.set(0,10,-10) 
    this.lightSun.position.set(0,10,-10)

    //! tareas reemplazadas por la  funcion createPlanet 
    // this.mercury.position.set(5,10,0)
    // this.mercuryObj.position.z = -10

    // this.saturno.position.set(10,12,0)
    // this.saturnoObj.position.z = -10

    // this.saturnoRing.position.set(10,10,0)
    // this.saturnoRing.rotation.x = -0.5 * Math.PI
    // -----------------------------------------------
  }


/**
 * !!!!! render  !!!!
 *
 * @private
 * @memberof BannerComponent
 */
  private startRenderingLoop() {

    //* Renderer
    // Use canvas element in template
    this.renderer = new THREE.WebGLRenderer({ canvas: this.canvas, antialias:true });
    this.renderer.setPixelRatio(devicePixelRatio);
    this.renderer.setSize(this.canvas.clientWidth, this.canvas.clientHeight); //iniciamos  las medidas

    //*shadows
    this.renderer.shadowMap.enabled = true 

    let component: BannerComponent = this;

    (function render() {
      requestAnimationFrame(render);
      // component.animateCube();
      component.animateSphere();
      component.spotLightOptions();
      component.rayCasterFunc();
      // component.animatedVertices()
      component.rotationPlanets()
      component.appyCannon()
      // component.animateCursor()
      // component.animatedCamera()
      component.handleControlFPC()
      component.renderer.render(component.scene, component.camera);
    }());

  }

  private starRender(){

    this.renderer = new THREE.WebGLRenderer({ canvas: this.canvas, antialias:true });
    this.renderer.setPixelRatio(devicePixelRatio);
    
    this.renderer.setSize(this.canvas.clientWidth, this.canvas.clientHeight);
    // this.renderer.domElement.style.position = 'absolute';
    // this.renderer.domElement.style.top = '0px';
    // this.rendererInDOM.appendChild(document.body, this.renderer.domElement);

    this.renderer.shadowMap.enabled = true 

    this.renderer.setAnimationLoop(()=>{
      this.animateSphere();
      this.spotLightOptions();
      this.rayCasterFunc();
      // component.animatedVertices()
      this.rotationPlanets()
      this.appyCannon()
      // component.animateCursor()
      // component.animatedCamera()
      if(!!this.controlsFPC){
        this.handleControlFPC()
      }
      this.renderer.render(this.scene, this.camera);
    });
  }


/**
 * !!!!! Controls  !!!!
 *
 * @private
 * @memberof BannerComponent
 */

  private createControls () {
    this.controls = new OrbitControls(this.camera, this.objRender.domElement);
    // this.controls.panSpeed = 2
    // this.controls.rotateSpeed = 2
    // this.controls.maxDistance = 50
    // this.controls.autoRotate = true;
    // this.controls.autoRotateSpeed= 5
    // this.controls.target = new THREE.Vector3(2,2,2)
    // this.controls.enableZoom = true;
    // this.controls.enablePan = false;
    // this.controls.enableDamping= true
    // this.controls.dampingFactor = 0.02
    // this.controls.mouseButtons.LEFT =  THREE.MOUSE.ROTATE
    // this.controls.mouseButtons.RIGHT = THREE.MOUSE.PAN
    this.controls.keys = {
      LEFT: 'ArrowLeft',
      UP: 'ArrowUp',
      RIGHT: 'ArrowRight',
      BOTTOM: 'ArrowBottom'
    }
    this.controls.listenToKeyEvents(window)
    this.controls.update();
  };

  private createControlsFPC(){
    this.controlsFPC = new FirstPersonControls(this.camera, this.objRender.domElement);
    this.controlsFPC.movementSpeed = 0.8
    this.controlsFPC.lookSpeed= 0.08
  }

  private createDatGUI () {

    const gui = new dat.GUI()
    gui.addColor(this.options,'sphereColor').onChange((c)=> this.sphere.material.color.set(c))
    gui.add(this.options, 'wireframe').onChange((w)=>this.sphere.material.wireframe = w)
    gui.add(this.options, 'speed',0,0.1)
    gui.add(this.options, 'angle',0,1)
    gui.add(this.options, 'penumbra',0,1)
    gui.add(this.options, 'intensity',0,1)

  }
  
  ngAfterViewInit() {
    this.createScene();
    // this.startRenderingLoop();
    this.starRender()
    // this.createControls();
    // this.createControlsFPC()
    // this.createDatGUI();
  }
  
  ngOnInit(): void {
    
    this.loadingManager.onProgress = (url,loaded,total) =>{
      let loadProgres = (loaded / total)*100
      this.spinner.show();
    }
    this.loadingManager.onLoad = () => {
      console.log('pagina cargada');
      this.spinner.hide();
    }
  }

  constructor(private rendererInDOM: Renderer2, private elemet: ElementRef, private spinner: NgxSpinnerService){

  }
}
