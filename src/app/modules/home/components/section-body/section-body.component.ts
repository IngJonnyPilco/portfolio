import { AfterContentInit, AfterViewInit, Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';


import { ScrollTrigger } from 'gsap/ScrollTrigger';
import { ScrollToPlugin } from 'gsap/ScrollToPlugin';
import { Observer } from 'gsap/Observer';
import { gsap } from "gsap";
import SplitType from 'split-type'
import { NativeElementsService } from '@shared/services/native-elements.service';


@Component({
  selector: 'app-section-body',
  templateUrl: './section-body.component.html',
  styleUrls: ['./section-body.component.scss']
})
export class SectionBodyComponent implements OnInit, AfterViewInit {
  @ViewChildren('mySection') public sections!: QueryList<ElementRef<HTMLElement>>;
  @ViewChildren('background') public images!: QueryList<ElementRef<HTMLElement>>;
  // @ViewChildren('section_heading') public headings!: QueryList<ElementRef<HTMLElement>>;
  @ViewChildren('outer') public outerWrappers!: QueryList<ElementRef<HTMLElement>>;
  @ViewChildren('inner') public innerWrappers!: QueryList<ElementRef<HTMLElement>>;

  @ViewChildren('example') public hola!: QueryList<ElementRef<HTMLElement>>
  // public splitHeadings!: SplitType[] 
  public wrap!: (index:number) => number;
  public currentIndex = -1
  public animating! : boolean

  constructor(private elementRef: ElementRef, private nativeElements : NativeElementsService) {
    gsap.registerPlugin(Observer)
  
  }
  
  ngAfterViewInit(): void {
    this.animateSections()
    this.observerGSAP()
  }
  
  public animateSections(){
  
    // this.splitHeadings = this.headings.toArray()
    // .map( (element: ElementRef) => new SplitType(element.nativeElement,{ types: 'words, chars, lines' }))

    this.wrap = gsap.utils.wrap(0, this.sections.toArray().length -1 )

    this.outerWrappers.toArray().forEach((element :ElementRef)=> {
      gsap.set(element.nativeElement,{ yPercent:100 });
    })
    
    this.innerWrappers.toArray().forEach((element :ElementRef)=> {
      gsap.set(element.nativeElement,{ yPercent:-100 });
    })

  }

  public goToSection(){
    !this.animating && this.gotoSection(this.currentIndex + 1, 1)
  }
  public observerGSAP(){
      Observer.create({
        type: "wheel,touch,pointer",
        wheelSpeed: -1,
        onDown: () => !this.animating && this.gotoSection(this.currentIndex - 1, -1),
        onUp: () => !this.animating && this.gotoSection(this.currentIndex + 1, 1),
        tolerance: 10,
        preventDefault: true
      });
      
      this.gotoSection(0, 1);
  }
  
  ngOnInit(): void {
  }

  private gotoSection(index :number, direction:number) {
    index = this.wrap(index); // make sure it's valid
    this.animating = true;

    let fromTop = direction === -1,
        dFactor = fromTop ? -1 : 1,
        tl = gsap.timeline({
          defaults: { duration: 1.25, ease: "power1.inOut" },
          onComplete: () => {this.animating = false}
        });
        
    if (this.currentIndex >= 0) {
      // The first time this function runs, current is -1
      
      gsap.set(this.sections.toArray()[this.currentIndex].nativeElement, { zIndex: 0 });
      tl.to(this.images.toArray()[this.currentIndex].nativeElement, { yPercent: -15 * dFactor })
        .set(this.sections.toArray()[this.currentIndex].nativeElement, { autoAlpha: 0 });
    }
    gsap.set(this.sections.toArray()[index].nativeElement, { autoAlpha: 1, zIndex: 1 });

    tl.fromTo(
      [this.outerWrappers.toArray()[index].nativeElement,
       this.innerWrappers.toArray()[index].nativeElement],
      { 
        yPercent: i => i ? -100 * dFactor : 100 * dFactor
      },
      { 
        yPercent: 0 },
        0
    )
      .fromTo(
        this.images.toArray()[index].nativeElement,
        { 
          yPercent: 15 * dFactor
        },
        { 
          yPercent: 0 
        },
         0
      )
      // .fromTo(this.splitHeadings[index].chars, { 
      //     autoAlpha: 0, 
      //     yPercent: 150 * dFactor
      // }, {
      //     autoAlpha: 1,
      //     yPercent: 0,
      //     duration: 1,
      //     ease: "power2",
      //     stagger: {
      //       each: 0.02,
      //       from: "random"
      //     }
      //   }, 0.2);
  
    this.currentIndex = index;
  }

}
