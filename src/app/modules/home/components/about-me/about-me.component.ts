import { AfterViewInit, Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import gsap from 'gsap';
import { ScrollTrigger } from 'gsap/ScrollTrigger';

// !!!!!!!!!!!https://codepen.io/GreenSock/pen/oNdNLxL?editors=1111
@Component({
  selector: 'app-about-me',
  templateUrl: './about-me.component.html',
  styleUrls: ['./about-me.component.scss']
})
export class AboutMeComponent implements OnInit,AfterViewInit {
  public posicionamiento : string = "../../../../../assets/svg/posicionamiento.svg"
  public arrowLeft : string = "../../.././../../assets/svg/left.svg"
  public arrowright : string = "../../.././../../assets/svg/right.svg"
  
  @ViewChildren('panel') public swipePanels! : QueryList<ElementRef<HTMLElement>>
  @ViewChildren('nav') public navigation! : QueryList<ElementRef<HTMLElement>>

  @ViewChild('bg') public bg! : ElementRef<HTMLElement>
  @ViewChild('ArrowBack') public arrowBack! : ElementRef<HTMLElement>
  @ViewChild('ArrowNext') public arrowNext! : ElementRef<HTMLElement>

  @ViewChildren('imgBrands') public brands! : QueryList<ElementRef<HTMLElement>>

  // public wrap!: (index:number) => number;

  constructor() { }
  
  ngOnInit(): void {
    gsap.registerPlugin(ScrollTrigger);
    // let intentObserver = this.observerGSAP()
    // ScrollTrigger.create({
    //   trigger: ".swipe-section",
    //   pin: true,
    //   start: "top top",
    //   end: "+=1",
    //   onEnter: () => {
    //     intentObserver.enable();
    //     this.gotoPanel(this.currentIndex + 1, true);    
    //   },
    //   onEnterBack: () => {
    //     intentObserver.enable();
    //     this.gotoPanel(this.currentIndex - 1, false);
    //   }
    // })
    
  }

  private currentIndex = 0;
  private animating!:boolean;
  private intentObserver! : Observer; 

  public numSections!:number; 

  ngAfterViewInit(): void {
    this.startPanels();
    this.animateSections();
    this.contentSections();

  }

  private animateSections(){

    // this.wrap = gsap.utils.wrap(0, this.swipePanels.toArray().length )
    // set second panel two initial 100%
    // gsap.set(".x", {xPercent:100});
      
    // set z-index levels for the swipe panels 
    this.swipePanels.forEach((panel, index)=> {
      gsap.set(panel.nativeElement, {
        zIndex: index+1
      });
    })
    // create an observer and disable it to start
    // this.intentObserver = this.observerGSAP()
    // this.intentObserver.disable();
  }
  private contentSections(){
    //*navigation
    this.navigation.toArray().forEach( (element)=> {
      gsap.set(element.nativeElement, {opacity:0, visibility:'hidden'});
    })

    //* image Backgrond
    gsap.set(this.bg.nativeElement, {xPercent:100})

    // *brands

  }

  private observerGSAP(){
    return ScrollTrigger.observe({
      type: "wheel,touch",
      onUp: () => {console.log('up')},
      onDown: () => {console.log('down')},
      // onUp: () => !this.animating && this.gotoPanel(this.currentIndex - 1, true),
      // onDown: () => !this.animating && this.gotoPanel(this.currentIndex + 1, false),
      tolerance: 10,
      preventDefault: true
    })
  }

  //*--------------- 
  public render : boolean = true
  private toggleNav(index:number){
    if(index == 0) {

      this.navigation.toArray().forEach( (element)=> {
        gsap.to(element.nativeElement, {opacity:0, visibility:'hidden'});
      })

      gsap.to(this.bg.nativeElement, {xPercent:100, duration:0.75})
      this.render = true
      return
    }

    if(index >0 && this.render) {
      this.render = false
     
      this.navigation.toArray().forEach( (element)=> {
        gsap.to(element.nativeElement, {
          opacity:1,
          duration:1,
          delay:0.75,
          visibility:'visible'
          });
      })
      gsap.to(this.bg.nativeElement, {
        xPercent:0,
        duration:0.75
        });
    }

  }
  
  private toggleBrands(index:number,element:HTMLElement, isScrollingDown :boolean) {

    const brandsBySections = element.querySelectorAll('figure')
    brandsBySections.forEach( imgs => {
      gsap.to(imgs, {
                opacity: isScrollingDown ? 1 : 0,
                duration:1,
                delay: isScrollingDown ? 0.75 :0,
                yPercent: isScrollingDown ? 100*index : 0,
                visibility:'visible',
                });
    })

  }
  //*--------------- 

  public gotoPanel(index: number, isScrollingDown:boolean) {
    


    if ((index === this.swipePanels.toArray().length  && isScrollingDown) || (index === -1 && !isScrollingDown)) {
      this.startPanels()
      this.toggleNav(0);
      return
    }


    this.toggleNav(index)
    this.animating = true;

    let target = isScrollingDown ? 
    this.swipePanels.toArray()[index].nativeElement : 
    this.swipePanels.toArray()[this.currentIndex].nativeElement;
    
    gsap.to(target, {
      xPercent: isScrollingDown ? 0 : 100,
      duration: 0.75,
      onComplete: () => {
        this.animating = false;
      }
    });

    this.toggleBrands(index,target,isScrollingDown)
    
    this.currentIndex = index;
  }

  public startPanels(){
    this.swipePanels.toArray().forEach( (panel,index) => {
      if(index>0){
        gsap.to(panel.nativeElement, {xPercent:100})
      }
    })
    this.currentIndex = 0
  }



  public next(){
    this.gotoPanel(this.currentIndex + 1, true)
  }

  public back(){
    this.gotoPanel(this.currentIndex -1, false)
  }

  public content: any[] = [ 
    {
      title: 'Diseño Web',
      features: [
        'Abode Figma',
        'Bizagi',
        'Desing Thinking',
        'Abode ilustrador',
        'Adobe Photoshop'
      ],
      brands: [
        '../../.././../../assets/icons/brands/figma-svgrepo-com.svg',
        '../../.././../../assets/icons/brands/bizagi_logo-w.svg',
        '../../.././../../assets/icons/brands/adobe-illustrator-svgrepo-com.svg',
        '../../.././../../assets/icons/brands/adobe-photoshop-svgrepo-com.svg',
      ]
    },
    {
      title: 'FrontEnd',
      features: [
        'Angular',
        'Astrojs',
        'TrheeJs',
        'Gsap',
        'Tailwindcss',
        'Material UI'
      ],
      brands: [
        '../../.././../../assets/icons/brands/angular-svgrepo-com.svg',
        '../../.././../../assets/icons/brands/astro.png',
        '../../.././../../assets/icons/brands//greensock-thumb.png.640b7d423125b0ad11e9f2af1cbf94c9.png',
        '../../.././../../assets/icons/brands/tailwindcss-icon-svgrepo-com.svg',
        '../../.././../../assets/icons/brands/material-ui-svgrepo-com.svg',
      ]
    },
    {
      title: 'Backend',
      features: [
        'Django',
        'Wordpress Handless',
        'NestJs',
        '.Net6 Core for API',
      ],
      brands: [
        '../../.././../../assets/icons/brands/django-svgrepo-com.svg',
        '../../.././../../assets/icons/brands/wordpress-svgrepo-com.svg',
        '../../.././../../assets/icons/brands/nestjs-svgrepo-com.svg',
        '../../.././../../assets/icons/brands/csharp-svgrepo-com.svg',
      ]
    },
    {
      title: 'Análisis de datos',
      features: [
        'Google Data Studio',
        'BigQuerry',
        'Python',
        'Sql',
        'NoSql'
      ],
      brands: [
        '../../.././../../assets/icons/brands/google-data-studio-svgrepo-com.svg',
        '../../.././../../assets/icons/brands/python-opened-svgrepo-com.svg',
        '../../.././../../assets/icons/brands/sql-database-sql-azure-svgrepo-com.svg',
        '../../.././../../assets/icons/brands/mongodb-svgrepo-com.svg',
      ]
    },
    {
      title: 'Colaboración',
      features: [
        'Git ',
        'GitLab - GitHub - Bitbucket',
        'Notion',
        'Slack',
        'Trello'
      ],
      brands: [
        '../../.././../../assets/icons/brands/git-svgrepo-com.svg',
        '../../.././../../assets/icons/brands/slack-svgrepo-com.svg',
        '../../.././../../assets/icons/brands/trello-color-svgrepo-com.svg',
      ]
    },
    {
      title: 'Metodologías ágiles',
      features: [
        'SCRUM',
        'Kaban',
        'Extremm Programming',
      ],
      brands: [
        '../../.././../../assets/icons/brands/scrum-svgrepo-com.svg',
        '../../.././../../assets/icons/brands/gui-back-svgrepo-com.svg',
      ]
    },
    {
      title: 'SEO',
      features: [
        'Google Analytics',
        'Google Search Console',
        'Hotjar',
        'Pixel Facebook',
        'YoastSeo'
      ],
      brands: [
        '../../.././../../assets/icons/brands/google-analytics-svgrepo-com.svg',
        '../../.././../../assets/icons/brands/google-data-studio-svgrepo-com.svg',
        '../../.././../../assets/icons/brands/hotjar-svgrepo-com.svg',
        '../../.././../../assets/icons/brands/facebook-1-svgrepo-com.svg',
        '../../.././../../assets/icons/brands/brand-yoast-svgrepo-com.svg',
        '../../.././../../assets/icons/brands/google-tag-manager-svgrepo-com.svg',
      ]
    },
  ]
}
