import { AfterViewInit, Component, ElementRef, HostListener, OnInit, QueryList, ViewChildren } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import gsap from 'gsap'
import { CardModalComponent } from '../card-modal/card-modal.component';
@Component({
  selector: 'app-my-services',
  templateUrl: './my-services.component.html',
  styleUrls: ['./my-services.component.scss']
})
export class MyServicesComponent implements OnInit,AfterViewInit {

  @ViewChildren("cards") public cards! : QueryList<ElementRef<HTMLElement>>
  constructor(private dialog: MatDialog) { }

  ngOnInit(): void {
  }
  ngAfterViewInit(): void {
    this.animaterCards()
  }

  public classes: Record<string, string[]> = {
    design: ['order-none','sm:col-span-2'],
    front: ['col-span-2','sm:col-span-5','sm:-order-1'],
    back: ['order-none','sm:col-span-2'],
    seo: ['-order-1', 'sm:order-none','sm:col-span-2'],
    analytics: ['order-none','sm:col-span-3'],
    asesor: ['order-none','sm:col-span-3'],
    education: ['order-none','sm:col-span-4'],
  };

  public animaterCards(){
    //*set atributtes
    this.cards.toArray().forEach((card,index) => {
      let elementCard = card.nativeElement.getAttribute("data-card")
      if (elementCard && this.classes[elementCard]) {
        card.nativeElement.classList.add(...this.classes[elementCard]);
      }
    })

  }

  public openModal($event:any){
    this.dialog.open(CardModalComponent,{
      data:$event,
      width: '100%',
      panelClass: 'custom-modalbox'
    });

  }

  
  public contentServices: any[] = [ 
    {
      title: 'Diseño Web',
      atribute: 'design',
      subtitle: '(Interfaces de usuario)',
      description: 'Diseño de aplicaciones UX/UI...',
      gallery: [
        'https://img.freepik.com/vector-gratis/plantilla-pagina-destino-comercial-marketing_23-2148958439.jpg',
        'https://img.freepik.com/vector-gratis/diseno-pagina-aterrizaje-musica-degradada_23-2149059581.jpg',
        'https://img.freepik.com/psd-gratis/explore-su-plantilla-pagina-destino-musica_23-2148641809.jpg',
        'https://img.freepik.com/vector-gratis/pagina-inicio-tecnologia-minima-diseno-plano_23-2149138461.jpg',
        'https://img.freepik.com/vector-gratis/plantilla-cabaret-diseno-plano-dibujado-mano_23-2149339905.jpg',
        'https://img.freepik.com/vector-gratis/plantilla-pagina-destino-escuela-baile-diseno-plano_23-2149414886.jpg'
      ],
      content:[
        'Metodología de diseño mediante Desing Thinking enfocado al UX/UI',
        'Interfaces modernas 100% personalizadas con la identidad de la marca.',
        'Metodología de trabajo alineado a los intereses del negocio.',
      ],
    },
    {
      title: 'Programación web',
      atribute: 'front',
      subtitle: '(Soluciones tecnológicas para emprendedores)',
      description: 'Páginas web, tiendas virtuales, academias online...',
      gallery: [
        'https://img.freepik.com/vector-gratis/pagina-inicio-compras-linea_33099-1725.jpg',
        'https://img.freepik.com/psd-gratis/concepto-paginas-aterrizaje-veterinarias_23-2148451973.jpg',
        'https://img.freepik.com/vector-gratis/plantilla-landing-page-negocios_23-2148254719.jpg',
        'https://img.freepik.com/vector-gratis/concepto-isometrico-aprendizaje-linea_1284-17947.jpg',
        'https://img.freepik.com/vector-gratis/plantilla-catalogo-productos-tecnologia-color-degradado_23-2149083928.jpg',
        'https://img.freepik.com/vector-gratis/plantilla-portafolio-degradado-creativo_52683-79240.jpg',
      ],
      content:[
        'Páginas web realizadas con tecnologias modernas',
        'Páginas de aterrizaje',
        'Sitios de comercios electrónicos para la venta de productos o servicios',
        'Academias educativas',
        'Catálogos de productos',
      ],
    },
    {
      title: 'Progamación backend',
      atribute: 'back',
      subtitle: '(Soluciones a nivel empresarial)',
      description: 'APIs, Automatización de procesos roboticos...',
      gallery: [
        'https://img.freepik.com/foto-gratis/pantalla-escaneo-programador-su-reloj-inteligente-camara-telefono-inteligente_1098-18710.jpg',
        'https://img.freepik.com/foto-gratis/concepto-rpa-pantalla-tactil-mano_23-2149311915.jpg',
        'https://img.freepik.com/vector-gratis/ilustracion-concepto-bot-chat_114360-5223.jpg',
      ],
      content:[
        'Desarrollo de interfaces de programación de aplicación APIS, para soluciones de comunicacion de aplicaciones',
        'Automatizacion de procesos roboticos mediante python',
        'Chatbot empresariales',
      ],
    },
    {
      title: 'SEO ',
      atribute: 'seo',
      subtitle: '(Posicionamiento en motores de busqueda)',
      description: 'Creación de contenido, Administracion de inventario...',
      gallery: [
        'https://img.freepik.com/vector-gratis/ilustracion-concepto-seo_114360-5846.jpg',
        'https://img.freepik.com/vector-gratis/personas-que-utilizan-cuadro-busqueda-consultas-motor-dando-resultado_74855-11000.jpg',
        'https://img.freepik.com/foto-gratis/lupa-conceptos-seo_1134-81.jpg',
        'https://img.freepik.com/vector-gratis/fondo-marketing-ordenador-e-iconos_23-2147584958.jpg',
      ],
      content:[
        'Creacion de contenido para posicionar tu negocio en internet',
        'Administracion de inventario en plataformas de comercio electrónico',
        'Informes periodicos del estado del negocio en internet',
      ],
    },
    {
      title: 'Análisis de datos',
      atribute: 'analytics',
      subtitle: '(Manejo de google analítica)',
      description: 'Representacion de estadísticas, informes...',
      gallery: [
        'https://img.freepik.com/foto-gratis/analisis-papel_1098-15678.jpg',
        'https://img.freepik.com/foto-gratis/concepto-reunion-trabajo-equipo-asunto-lanzamiento_1421-194.jpg',
        'https://img.freepik.com/foto-gratis/hombre-negocios-haciendo-presentacion-sus-colegas-estrategia-negocio-efecto-capa-digital-oficina-como-concepto_1423-123.jpg',
      ],
      content:[
        'Extracción de datos de google Analytics',
        'Analítica, limpieza y carga de datos',
        'Uso de data estudio para la presentación de informes para la toma de decisiones',
      ],
    },
    {
      title: 'Asesoramiento web',
      atribute: 'asesor',
      subtitle: '(Manejo de platafomas)',
      description: 'Problemas en el manejo de Wordpress y otras tecnologías..',
      gallery: [
        'https://img.freepik.com/foto-gratis/dos-empresarios-discutiendo-detalles-contrato_1163-3971.jpg',
        'https://img.freepik.com/foto-gratis/cerrar-hombre-escribiendo-codigo-computadora-portatil_158595-5169.jpg',
        'https://img.freepik.com/foto-gratis/concepto-web-tecnologia-avanzada-programacion-html_53876-120338.jpg',
        'https://img.freepik.com/vector-gratis/ilustracion-concepto-marcos-javascript_114360-4699.jpg',

      ],
      content:[
        'Asesoramiento para el manejo de plataformas de comercio electónico',
        'Asesoramiento para el uso de tecnologias FullStack',
        'Despliegue de aplicaciones en la nube',
      ],
    },
    {
      title: 'Clases particulares',
      atribute: 'education',
      subtitle: '(educación)',
      description: 'Lógica de programación, typescript, python...',
      gallery: [
        'https://img.freepik.com/foto-gratis/dos-empresarios-discutiendo-detalles-contrato_1163-3971.jpg',
        'https://img.freepik.com/foto-gratis/cerrar-hombre-escribiendo-codigo-computadora-portatil_158595-5169.jpg',
        'https://img.freepik.com/foto-gratis/concepto-web-tecnologia-avanzada-programacion-html_53876-120338.jpg',
        'https://img.freepik.com/vector-gratis/ilustracion-concepto-marcos-javascript_114360-4699.jpg',

      ],
      content:[
        'Clases de programación desde 0 en javascript, typescript, python, c#.',
        'Proyectos estudiantiles',
        'Asesoramiento en ideas de negocio',
      ],
    },

  ]
}
