import { Component, OnInit,Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card-modal',
  templateUrl: './card-modal.component.html',
  styleUrls: ['./card-modal.component.scss']
})
export class CardModalComponent implements OnInit {

  private messaje = 'Hola, me interesa conocer mas sobre el servicio de'
  public PhoneNumber = 593962757366;
  public linkWhatsapp : SafeResourceUrl = ``

  constructor(@Inject(MAT_DIALOG_DATA) public dataCard: any, private sanitization: DomSanitizer, private router: Router) { }

  ngOnInit(): void {
    this.generateLink()
  }
  
  actionButton($event:any){
  }

  generateLink() {
    const smsInitial: string = encodeURIComponent(this.messaje)
    const url =`https://wa.me/${this.PhoneNumber}?text=${smsInitial}`;
    const urlService = ` ${this.dataCard.title}`;
    const urlFinal = url.concat(encodeURIComponent(urlService))
    this.linkWhatsapp = this.sanitization.bypassSecurityTrustResourceUrl(urlFinal)
  }
}
