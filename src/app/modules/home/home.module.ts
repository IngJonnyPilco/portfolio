import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { PageComponent } from './page/page.component';
import { BannerComponent } from './components/banner/banner.component';
import { HomeLayoutComponent } from './home-layout/home-layout.component';
import { SharedModule } from '@shared/shared.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { JonnyProfileComponent } from './components/jonny-profile/jonny-profile.component';
import { SectionBodyComponent } from './components/section-body/section-body.component';
import { PresentationComponent } from './components/presentation/presentation.component';
import { AboutMeComponent } from './components/about-me/about-me.component';
import { MyServicesComponent } from './components/my-services/my-services.component';
import { MyWorkComponent } from './components/my-work/my-work.component';
import { CardModalComponent } from './components/card-modal/card-modal.component';
import { CotizationLayoutComponent } from './cotization-layout/cotization-layout.component';


@NgModule({
  declarations: [
    PageComponent,
    BannerComponent,
    HomeLayoutComponent,
    JonnyProfileComponent,
    SectionBodyComponent,
    PresentationComponent,
    AboutMeComponent,
    MyServicesComponent,
    MyWorkComponent,
    CardModalComponent,
    CotizationLayoutComponent,
    
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule,
    NgxSpinnerModule.forRoot({ type: 'ball-scale-multiple' })
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class HomeModule { }
