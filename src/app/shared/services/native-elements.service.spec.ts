import { TestBed } from '@angular/core/testing';

import { NativeElementsService } from './native-elements.service';

describe('NativeElementsService', () => {
  let service: NativeElementsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NativeElementsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
