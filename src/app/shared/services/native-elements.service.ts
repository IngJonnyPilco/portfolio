import { ElementRef, Injectable, QueryList } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NativeElementsService {

  constructor() { }

  public listAngularElements(arrayElements:QueryList<ElementRef>) : HTMLElement[]{
    return arrayElements.map( (element : ElementRef) => element.nativeElement )
  }
}
