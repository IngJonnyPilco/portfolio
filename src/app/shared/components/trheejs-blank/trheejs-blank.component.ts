import { AfterViewInit, Component, ElementRef, HostListener, Input, OnInit, Renderer2, ViewChild } from '@angular/core';
import * as THREE from "three";
import * as dat from "dat.gui";
import * as CANNON from "cannon-es"


import gsap from 'gsap';

import { GLTFLoader, GLTF } from 'three/examples/jsm/loaders/GLTFLoader.js';
import { RGBELoader } from 'three/examples/jsm/loaders/RGBELoader';
import { FontLoader } from 'three/examples/jsm/loaders/FontLoader';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import { FirstPersonControls } from 'three/examples/jsm/controls/FirstPersonControls';
import { CSS2DRenderer } from 'three/examples/jsm/renderers/CSS2DRenderer.js';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-trheejs-blank',
  templateUrl: './trheejs-blank.component.html',
  styleUrls: ['./trheejs-blank.component.scss']
})
export class TrheejsBlankComponent implements OnInit, AfterViewInit {


  @ViewChild('canvasElement') private canvasRef!: ElementRef<HTMLCanvasElement>; //#canvas
  
  constructor(private rendererInDOM: Renderer2, private elemet: ElementRef, private spinner: NgxSpinnerService){
  }
  
  //* elements 
  @Input() public estrellas: string = "/assets/img-solar-sistem/stars.jpg";
  //* Camera Properties 
  @Input() public fieldOfView: number = 75; // FOV is the extent of the scene that is seen on the display at any given moment. The value is in degrees.
  @Input('nearClipping') public nearClippingPlane: number = 0.1;
  @Input('farClipping') public farClippingPlane: number = 1000;

  //* Lights 
  private ambientLight!: THREE.AmbientLight;
  private lightSun!: THREE.PointLight;
  private directionalLight!: THREE.DirectionalLight;

  private spotLight!: THREE.SpotLight

  //*LightsHelpers
  private dirLightHelper!: THREE.DirectionalLightHelper
  private dirLightShadowHelper!: THREE.CameraHelper
  private spotLightHelper!: THREE.SpotLightHelper

  //*helpers
  private axesHelpers  =  new THREE.AxesHelper(10)
  private gridHelper = new THREE.GridHelper(10,10)

  //* controls


  //*------------------------------------*variables para asignar--------------------------------------------------
  private camera!: THREE.PerspectiveCamera;
  private loadingManager: THREE.LoadingManager = new THREE.LoadingManager()
  
  private textureLoader : THREE.TextureLoader = new THREE.TextureLoader()
  private cubeTextureLoader : THREE.CubeTextureLoader = new THREE.CubeTextureLoader() 
  
  private gltfloader: GLTFLoader = new GLTFLoader(this.loadingManager);
  private renderer!: THREE.WebGLRenderer;
  private scene: THREE.Scene = new THREE.Scene();
  

  //*------------------------------------*Objetos----------------------------------------------------------------

  private planeMesh = new THREE.Mesh(
    new THREE.PlaneGeometry(1,1),
    new THREE.MeshStandardMaterial({ 
      color: 0xffffff, side: THREE.DoubleSide 
    })
    );

  private cubeMesh = new THREE.Mesh(
    new THREE.BoxGeometry(1, 1, 1), 
    new THREE.MeshBasicMaterial({
      color: 0x00ff00
    })
    );


  //*------------------------------------*Métodos----------------------------------------------------------------
  private get canvas(): HTMLCanvasElement {
    return this.canvasRef.nativeElement;
  }
  
  private getAspectRatio() {
    return this.canvas.clientWidth / this.canvas.clientHeight;
  }
  //*------------------------------------*Host Listener-----------------------------------------------------------
  @HostListener('window:resize') onResizeEvent(){
    this.camera.aspect = this.getAspectRatio()
    this.camera.updateProjectionMatrix();
    this.renderer.setSize(window.innerWidth , window.innerHeight); //actualizamos  las medidas
    // this.controls.update()
  }
  
  //*------------------------------------*Scene-------------------------------------------------------------------
  private createScene(){
    this.scene.background = new THREE.Color(0x000000)

    //____________________________________ 
    let aspectRatio = this.getAspectRatio();
    this.camera = new THREE.PerspectiveCamera( this.fieldOfView, aspectRatio, this.nearClippingPlane,this.farClippingPlane)
    this.camera.position.x= 0 // +izquierda - derecha
    this.camera.position.y= 10 // +arriba - abajo
    this.camera.position.z= 10 // +atras - adelante
    this.camera.lookAt(new THREE.Vector3(0, 0, 0)); //hacia donde queremos que vea
    //____________________________________ 
    //* add objects

    // this.scene.add(this.planeMesh, this.cubeMesh)

    //* add helpers
    this.scene.add(this.axesHelpers, this.gridHelper)
  }
  
  //*------------------------------------*Render-------------------------------------------------------------------
  private starRender(){

    this.renderer = new THREE.WebGLRenderer({ canvas: this.canvas, antialias:true });
    this.renderer.setPixelRatio(devicePixelRatio);
    this.renderer.setSize(this.canvas.clientWidth, this.canvas.clientHeight);

    this.renderer.setAnimationLoop(()=>{
      this.renderer.render(this.scene, this.camera);
    });
  }

  ngAfterViewInit() {
    this.createScene();
    this.starRender()
  }
  
  ngOnInit(): void {
    this.loadingManager.onProgress = (url,loaded,total) =>{
      let loadProgres = (loaded / total)*100
      this.spinner.show();
    }
    this.loadingManager.onLoad = () => {
      console.log('pagina cargada');
      this.spinner.hide();
    } 
  }


}
