import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrheejsBlankComponent } from './trheejs-blank.component';

describe('TrheejsBlankComponent', () => {
  let component: TrheejsBlankComponent;
  let fixture: ComponentFixture<TrheejsBlankComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrheejsBlankComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TrheejsBlankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
