import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import gsap from 'gsap';
interface NavigationMenu { 
  navigation: string,
  icon: string,
  router: string[]
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public menuOptions: NavigationMenu[] = [
    { 
      navigation: 'Sobre mi',
      icon: 'home',
      router: [`/`],

    },
    { 
      navigation: 'Mi trabajo',
      icon: 'home',
      router: [`/projects`],
    },
    { 
      navigation: 'Cotiza tu idea',
      icon: 'home',
      router: [`/cotization`],
    },
    { 
      navigation: 'Contáctame',
      icon: 'home',
      router: [`/`],
    }
  ]
  
  @ViewChild('nav',{static:true}) nav!: ElementRef<HTMLElement>;
  @ViewChild('menu',{static:true}) menu!: ElementRef<HTMLElement>;
  @ViewChild('nav_togle',{static:true}) menuToggle!: ElementRef<HTMLElement>;

  public isMenuOpen = false;
  constructor() { }

  ngOnInit(): void {
    
  }
  //* TOGGLE MENU ACTIVE STATE
  button_togle(){
    this.isMenuOpen =!this.isMenuOpen;
    this.menuToggle.nativeElement.setAttribute('aria-expanded', String(this.isMenuOpen));
    this.menu.nativeElement.hidden = !this.isMenuOpen;
    this.nav.nativeElement.classList.toggle('nav--open');

    if(this.isMenuOpen)this.animationIn()

  }

  public animationIn(){ 
    gsap.set(this.menu.nativeElement, {opacity: 0});
    gsap.to(this.menu.nativeElement, {
        duration: 1.5,
        stagger: {
            each: 0.1, // delay of 100ms between each letter
            from: "start"
        },
        opacity: 1,
        delay: 0.7
    });
  }


}
