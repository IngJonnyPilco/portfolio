import { Component, OnInit } from '@angular/core';
interface NavigationMenu { 
  navigation: string,
  icon: string,
}
@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
  public socialMediaOptions: NavigationMenu[] = [
    { 
      navigation: 'LinkedIn',
      icon: '/assets/icons/linkedin-svgrepo-com.svg',
    },
    { 
      navigation: 'GitLab',
      icon: '/assets/icons/gitlab-svgrepo-com.svg',
    },
    { 
      navigation: 'E-mail',
      icon: '/assets/icons/mail-svgrepo-com.svg',
    }
  ]
}
