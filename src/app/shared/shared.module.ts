import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { RouterModule } from '@angular/router';

import { HttpClientModule } from '@angular/common/http';
import { MaterialUiModule } from './material-ui/material-ui.module';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { TrheejsBlankComponent } from './components/trheejs-blank/trheejs-blank.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { PrimaryButtonComponent } from './components/button/primary-button.component';

const SharedComponents = [
  HeaderComponent,
  FooterComponent,
  PrimaryButtonComponent
]

const SharedModules = [
  CommonModule,
  RouterModule,
  HttpClientModule,
  MaterialUiModule,
  NgOptimizedImage,

]



@NgModule({
  declarations: [
    ...SharedComponents,
    TrheejsBlankComponent,
  ],
  imports: [
    ...SharedModules,
    NgxSpinnerModule.forRoot({ type: 'ball-scale-multiple' })
  ],
  exports:[
    ...SharedComponents,
    ...SharedModules
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class SharedModule { }
